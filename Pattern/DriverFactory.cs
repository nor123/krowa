﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace Pattern
{
    public static class DriverFactory
    {
        static IWebDriver driver;

        public static IWebDriver GetDriver()
        {
            string driver  = ConfigurationManager.AppSettings["driver"];
            Enum.TryParse(driver, out DriverName dirverName);

            switch (dirverName)
            {
                case DriverName.Chrome:
                    return new ChromeDriver();
                case DriverName.Firefox:
                    return new FirefoxDriver();
                case DriverName.Ie:
                    return new InternetExplorerDriver();
                default:
                    return new ChromeDriver();
            }
        }
    }

    public enum DriverName
    {
        Chrome, Firefox, Ie
    }
}
