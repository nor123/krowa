﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern.Pages
{
    public class LoginPage
    {
        public Utils utils = new Utils();

        private string UserNameXpath = "//input[@id='txtUsername']";
        private string UserPasswordXpath = "//input[@id='txtPassword']";
        private string LoginButtonXpath = "//input[@id='btnLogin']";
        private string WelcomeAdminXpath = "//a[@id='welcome']";
        private string OrangePage = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";

        public void GoToOrangePage()
        {
            utils.GoToUrl(OrangePage);
        }

        public void PutUserName(string userName)
        {
            utils.SendKeys(UserNameXpath, TimeSpan.FromSeconds(4), "login page - username field", userName);
        }

        public void PutPasswordName(string password)
        {
            utils.SendKeys(UserPasswordXpath, TimeSpan.FromSeconds(3), "password field", password);
        }

        public void ClickLoginButton()
        {
            utils.ClickButton(LoginButtonXpath, TimeSpan.FromSeconds(3), "login button");
        }

        public void FindWelcomeAdmibn()
        {
            utils.FindElement(WelcomeAdminXpath, TimeSpan.FromSeconds(3), "welcome admin sign");
        }
    }
}
