﻿using Pattern.Pages;
using System;
using TechTalk.SpecFlow;

namespace Pattern
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        LoginPage loginPage;

        public SpecFlowFeature1Steps()
        {
            loginPage = new LoginPage();
        }

        [Given(@"I go to Orange Page")]
        public void GivenIGoToOrangePage()
        {
            loginPage.GoToOrangePage();
        }

        [Given(@"I have entered (.*) in the userName field")]
        public void GivenIHaveEnteredAdminInTheUserNameField(string userName)
        {
            loginPage.PutUserName(userName);
        }
        
        [Given(@"I have entered (.*) in the password field")]
        public void GivenIHaveEnteredPasswordInThePasswordField(string password)
        {
            loginPage.PutPasswordName(password);
        }
        
        [When(@"I press Login")]
        public void WhenIPressLogin()
        {
            loginPage.ClickLoginButton();
        }
        
        [Then(@"I should be logged in and the WelcomAdmin is visible")]
        public void ThenIShouldBeLoggedInAndTheWelcomAdminIsVisible()
        {
            loginPage.FindWelcomeAdmibn();
        }
    }
}
