﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;

namespace Pattern
{
    public class Utils
    {
        IWebDriver driver;

        public Utils()
        {
            driver = DriverFactory.GetDriver();
        }

        public void GoToUrl(string url)
        {
            driver.Url = url;
        }

        public void ClickButton(string xpath, TimeSpan timeSpan, string elementName)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeSpan);
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(xpath))).Click();
            }
            catch
            {
                Console.WriteLine($"Not found element. Was searching button: {elementName}");
                //throw new Exception($"Not found element. Was searching button: {elementName}");
            }
        }

        public void SendKeys(string xpath, TimeSpan timeSpan, string elementName, string keys)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeSpan);
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(xpath))).SendKeys(keys);
            }
            catch
            {
                Console.WriteLine($"Not found element. Was searching button: {elementName}");
            }
        }

        public bool FindElement(string xpath, TimeSpan timeSpan, string elementName)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeSpan);
            try
            {
                IWebElement element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
                if(element != null)
                {
                    return true;
                }
            }
            catch
            {
                Console.WriteLine($"Not found element. Was searching button: {elementName}");
                return false;
            }

            return false;
        }
    }
}
