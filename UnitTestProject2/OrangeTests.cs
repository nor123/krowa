﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace UnitTestProject2
{
    [TestFixture]
    public class OrangeTests
    {
    //my comments
        IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
        }

        [Test, Order(2)]
        public void Logowanie()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
            driver.FindElement(By.XPath("//input[@id='txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id='txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id='btnLogin']")).Click();
            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id='welcome']"));

        }

        [Test, Order(1)]
        public void DashboardClick()
        {
            driver.Url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
            driver.FindElement(By.XPath("//input[@id='txtUsername']")).SendKeys("Admin");
            driver.FindElement(By.XPath("//input[@id='txtPassword']")).SendKeys("admin123");
            driver.FindElement(By.XPath("//input[@id='btnLogin']")).Click();
            IWebElement welcomeAdmin = driver.FindElement(By.XPath("//a[@id='welcome']"));

            Assert.IsNotNull(welcomeAdmin);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
        }
    }
}
